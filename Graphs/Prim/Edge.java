package Prim;

public class Edge {
    public int w = 1;
    public Vertex vertexFrom;
    public Vertex vertexTo;

    public Edge(Vertex v1, Vertex v2) {
        this.vertexFrom = v1;
        this.vertexTo = v2;
    }

    public Edge(Vertex v1, Vertex v2, int w) {
        this(v1, v2);
        this.w = w;
    }
    
    public String toString() {
        return String.format("Edge between %s(w:%d) and %s(w:%d), weight: %d", vertexFrom.name, vertexFrom.key, vertexTo.name, vertexTo.key, w);
    }
}
