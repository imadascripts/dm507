package Prim;

import java.util.List;
import java.util.LinkedList;

public class Vertex implements Comparable<Vertex>{
    public List<Edge> adj;
    public int key, weight;
    public String name; 
    public Vertex parent;
    static int names = 64;

    public Vertex() {
        this.adj = new LinkedList<>();
        name = String.valueOf((char)++names);
    }

    @Override
    public int compareTo(Vertex o) {
        int r = this.key-o.key;
        if (r == 0) {
            r = this.name.compareTo(o.name);
        }
        return r;
    }
    
    @Override
    public String toString() {
        return String.format("Vertex: %s - %d with Path: %s", name, key, parent != null? parentString():"0");
    }
    
    public String parentString() {
        return String.format("%s %s" , name, parent != null? "<- "+parent.parentString():"");
    }
}
