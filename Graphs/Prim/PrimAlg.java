package Prim;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class PrimAlg {

    public static void main(String[] args) {
        Graph graph = new Graph();
        graph.addVertices(9);
        graph.incident(0, 1, 4); // a -> b
        graph.incident(1, 2, 2); // b -> c
        graph.incident(2, 3, 11); // c -> d
        graph.incident(3, 4, 21); // d -> e
        graph.incident(4, 5, 15); // e -> f
        graph.incident(5, 6, 10); // f -> g
        graph.incident(6, 7, 9); // g -> h
        graph.incident(7, 0, 8); // h -> a
        graph.incident(8, 0, 7); // i -> a
        graph.incident(8, 1, 6); // i -> b
        graph.incident(8, 2, 22); // i -> c
        graph.incident(8, 3, 18); // i -> d
        graph.incident(8, 4, 14); // i -> e
        graph.incident(8, 5, 5); // i -> f
        graph.incident(8, 6, 13); // i -> g
        graph.incident(8, 7, 17); // i -> h

        ArrayList<Vertex> E = prim(graph, graph.vertices.get(0));
        for (Vertex v : E) {
            System.out.println(v);
        }
    }

    public static ArrayList prim(Graph G, Vertex r) {
        ArrayList<Vertex> E = new ArrayList();
        for (Vertex u : G.vertices) {
            u.key = Integer.MAX_VALUE;
            u.parent = null;
        }
        r.key = 0;
        PriorityQueue<Vertex> pq = new PriorityQueue<>();
        for (Vertex x : G.vertices) {
            pq.add(x);
        }
        Vertex u, x = null;
        while (!pq.isEmpty()) {
            u = pq.poll();
            System.out.println("udtag:" + u);
            E.add(u);
            for (Edge e : u.adj) {
                Vertex v = e.vertexTo;
                if (pq.contains(v) && e.w < v.key) {
                    pq.remove(v);
                    v.parent = u;
                    v.key = e.w;
                    pq.add(v);
                }
            }

            x = u;
        }
        return E;
    }

}
