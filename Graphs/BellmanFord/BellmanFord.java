package BellmanFord;

public class BellmanFord {

    public static void main(String[] args){

        DiGraph g = new DiGraph();
        g.add("a", "b", 17);
        g.add("a", "e", 8);
        g.add("a", "f", 10);
        g.add("b", "g", -5);
        g.add("c", "b", 19);
        g.add("c", "d", 2);
        g.add("d", "e", 6);
        g.add("e", "h", -4);
        g.add("f", "g", 25);
        g.add("g", "c", -3);
        g.add("g", "h", -12);
        g.add("h", "d", 1);
        g.add("h", "f", -10);

      
//        g.add("s", "t", 6);
//        g.add("s", "y", 7);
//        g.add("t", "x", 5);
//        g.add("t", "y", 8);
//        g.add("t", "z", -4);
//        g.add("x", "t", -2);
//        g.add("y", "x", -3);
//        g.add("y", "z", 9);
//        g.add("z", "s", 2);
//        g.add("z", "x", 7);
        
        System.out.println("This graph: " + g);

        boolean returnVal = BellmanFord(g, g.getVertex("a"));
        System.out.println(returnVal);
        System.out.println("This graph: " + g);
    }

    public static void initializeSingleSource(DiGraph g, Vertex s) {
        for (Vertex v : g.neighbors.keySet()) {
            v.d = Integer.MAX_VALUE/2;
        }
        s.d = 0;
    }

    public static void relax(DiGraph g, Vertex u, Vertex v) {
        if (v.d > (u.d + g.getCost(u, v))) {
            v.d = u.d + g.getCost(u, v);
            v.parent = u;
        }
    }

    public static boolean BellmanFord(DiGraph g, Vertex s) {
        initializeSingleSource(g, s);
        int ii=0;
        for (int i = 1; i < g.getNumberVertices(); i++) {
            int jj=0;
            for (DiGraph.Edge e : g.edges) {
                System.out.printf("t:%d, c:%d\n", ++ii,++jj);
                relax(g, e.getVertexFrom(), e.getVertexTo());
            }
        }
        for (DiGraph.Edge e : g.edges) {
            if (e.getVertexTo().d > (e.getVertexFrom().d + g.getCost(e.getVertexFrom(), e.getVertexTo()))) {
                return false;
            }
        }
        return true;
    }

}
