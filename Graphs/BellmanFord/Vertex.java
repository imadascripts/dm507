package BellmanFord;

import java.util.List;
import java.util.LinkedList;
import java.util.Objects;

public class Vertex {
    public List<Edge> adj;
    public int p, d, f;
    public Vertex parent;
    public String name; 
    private int recursionDepth;
    static int names = 64;

    public Vertex(String name) {
        this.adj = new LinkedList<>();
        this.name = name;
    }
    
    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vertex other = (Vertex) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        recursionDepth = 0;
        return String.format("(%s, %d) ", name, d, parent != null? new StringBuilder(parentString()).reverse():"X_X");
    }
    
    public String parentString() {
        if(recursionDepth++ < 100) {
            return String.format("%s %s" , name, parent != null? "→ " + parent.parentString():"");
        }
        else {
            return String.format("dehcaer timiL %s", name);
        }
    }
}
