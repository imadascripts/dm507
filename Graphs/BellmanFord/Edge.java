package BellmanFord;

public class Edge {
    public int w = 1;
    public Vertex vertexFrom;
    public Vertex vertexTo;

    public Edge(Vertex v1, Vertex v2) {
        this.vertexFrom = v1;
        this.vertexTo = v2;
    }

    public Edge(Vertex v1, Vertex v2, int w) {
        this(v1,v2);
        this.w = w;
    }
}
