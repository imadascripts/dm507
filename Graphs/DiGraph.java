import java.io.IOException;
import java.util.*;

public class DiGraph<Vertex> {

    public static class Edge<Vertex>{
        private Vertex vertex;
        private int cost;

        public Edge(Vertex v, int c){
            vertex = v; cost = c;
        }

        public Vertex getVertex() {
            return vertex;
        }

        public int getCost() {
            return cost;
        }

        @Override
        public String toString() {
            return "Edge [vertex=" + vertex + ", cost=" + cost + "]";
        }

    }

    /**
     * A Map is used to map each vertex to its list of adjacent vertices.
     */

    public Map<Vertex, List<Edge<Vertex>>> neighbors = new HashMap<>();

    private int nr_edges;

    /**
     * String representation of graph.
     */
    public String toString() {
        StringBuffer s = new StringBuffer();
        for (Vertex v : neighbors.keySet())
            s.append("\n    " + v + " -> " + neighbors.get(v));
        return s.toString();
    }

    /**
     * Add a vertex to the graph. Nothing happens if vertex is already in graph.
     */
    public void add(Vertex vertex) {
        if (neighbors.containsKey(vertex))
            return;
        neighbors.put(vertex, new ArrayList<Edge<Vertex>>());
    }

    public int getNumberOfEdges(){
        int sum = 0;
        for(List<Edge<Vertex>> outBounds : neighbors.values()){
            sum += outBounds.size();
        }
        return sum;
    }

    /**
     * True iff graph contains vertex.
     */
    public boolean contains(Vertex vertex) {
        return neighbors.containsKey(vertex);
    }

    /**
     * Add an edge to the graph; if either vertex does not exist, it's added.
     * This implementation allows the creation of multi-edges and self-loops.
     */
    public void add(Vertex from, Vertex to, int cost) {
        this.add(from);
        this.add(to);
        neighbors.get(from).add(new Edge<Vertex>(to, cost));
    }

    public int outDegree(int vertex) {
        return neighbors.get(vertex).size();
    }

    public int inDegree(Vertex vertex) {
       return inboundNeighbors(vertex).size();
    }

    public List<Vertex> outboundNeighbors(Vertex vertex) {
        List<Vertex> list = new ArrayList<Vertex>();
        for(Edge<Vertex> e: neighbors.get(vertex))
            list.add(e.vertex);
        return list;
    }

    public List<Vertex> inboundNeighbors(Vertex inboundVertex) {
        List<Vertex> inList = new ArrayList<Vertex>();
        for (Vertex to : neighbors.keySet()) {
            for (Edge e : neighbors.get(to))
                if (e.vertex.equals(inboundVertex))
                    inList.add(to);
        }
        return inList;
    }

    public boolean isEdge(Vertex from, Vertex to) {
      for(Edge<Vertex> e :  neighbors.get(from)){
          if(e.vertex.equals(to))
              return true;
      }
      return false;
    }

    public int getCost(Vertex from, Vertex to) {
        for(Edge<Vertex> e :  neighbors.get(from)){
            if(e.vertex.equals(to))
                return e.cost;
        }
        return -1;
    }
    
    public int getNumberVertices() {
        return neighbors.keySet().size();
    }

    public static void main(String[] args) throws IOException {

        DiGraph<Integer> graph = new DiGraph<Integer>();

        graph.add(1);
        graph.add(2);
        graph.add(3);
        graph.add(4);
        graph.add(5);
        graph.add(6);
        graph.add(7);
        graph.add(8);
        graph.add(0);

        graph.add(2, 1, 1);
        graph.add(2, 3, 1);
        graph.add(4, 3, 1);
        graph.add(5, 4, 1);
        graph.add(6, 5, 1);
        graph.add(7, 6, 1);
        graph.add(8, 7, 1);
        graph.add(8, 1, 1);
        graph.add(1, 0, 1);
        graph.add(3, 0, 1);
        graph.add(5, 0, 1);
        graph.add(6, 0, 1);
        graph.add(7, 0, 1);
        graph.add(0, 2, 1);
        graph.add(0, 4, 1);
        graph.add(0, 8, 1);

        System.out.println("The nr. of vertices is: " + graph.getNumberVertices());
        System.out.println("The nr. of edges is: " + graph.getNumberOfEdges()); // to be fixed
        System.out.println("The current graph: " + graph);
        System.out.println("In-degrees for 0: " + graph.inDegree(0));
        System.out.println("Out-degrees for 0: " + graph.outDegree(0));
        System.out.println("In-degrees for 3: " + graph.inDegree(3));
        System.out.println("Out-degrees for 3: " + graph.outDegree(3));
        System.out.println("Outbounds for 1: "+ graph.outboundNeighbors(1));
        System.out.println("Inbounds for 1: "+ graph.inboundNeighbors(1));
        System.out.println("(0,2)? " + (graph.isEdge(0, 2) ? "It's an edge" : "It's not an edge"));
        System.out.println("(1,3)? " + (graph.isEdge(1, 3) ? "It's an edge" : "It's not an edge"));

        System.out.println("Cost for (1,3)? "+ graph.getCost(1, 3));


    }
}