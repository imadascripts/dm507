import java.util.*;

public class BFS {
    private static final int INFINITY = Integer.MAX_VALUE;
    private boolean[] marked;  // marked[v] = is there an s->v path?
    private Integer[] edgeTo;      // edgeTo[v] = last edge on shortest s->v path
    private int[] distTo;      // distTo[v] = length of shortest s->v path

    /**
     * Computes the shortest path from <tt>s</tt> and every other vertex in graph <tt>G</tt>.
     * @param G the digraph
     * @param s the source vertex
     */
    public BFS(DiGraph<Integer> G, Integer s) {
        marked = new boolean[G.getNumberVertices()];
        distTo = new int[G.getNumberVertices()];
        edgeTo = new Integer[G.getNumberVertices()];
        for (int v = 0; v < G.getNumberVertices(); v++) distTo[v] = INFINITY;
        bfs(G, s);
    }

    // BFS from single source
    private void bfs(DiGraph<Integer> G, Integer s) {
        Queue<Integer> q = new Queue<Integer>();
        marked[s] = true;
        distTo[s] = 0;
        q.enqueue(s);
        while (!q.isEmpty()) {
            int v = q.dequeue();
            System.out.println(v);
            for (Integer w : G.outboundNeighbors(v)) {
                if (!marked[w]) {
                    if (G.isEdge(v, w))
                        edgeTo[w] = v;
                    distTo[w] = distTo[v] + 1;
                    marked[w] = true;
                    q.enqueue(w);
                }
            }
        }
    }

    /**
     * Is there a directed path from the source <tt>s</tt> (or sources) to vertex <tt>v</tt>?
     * @param v the vertex
     * @return <tt>true</tt> if there is a directed path, <tt>false</tt> otherwise
     */
    public boolean hasPathTo(int v) {
        return marked[v];
    }

    /**
     * Returns the number of edges in a shortest path from the source <tt>s</tt>
     * (or sources) to vertex <tt>v</tt>?
     * @param v the vertex
     * @return the number of edges in a shortest path
     */
    public int distTo(int v) {
        return distTo[v];
    }

    /**
     * Returns a shortest path from <tt>s</tt> (or sources) to <tt>v</tt>, or
     * <tt>null</tt> if no such path.
     * @param v the vertex
     * @return the sequence of vertices on a shortest path, as an Iterable
     */
    public Iterable<Integer> pathTo(int v) {
        if (!hasPathTo(v)) return null;
        Stack<Integer> path = new Stack<Integer>();
        int x;
        for (x = v; distTo[x] != 0; x = edgeTo[x])
            path.push(x);
        path.push(x);
        return path;
    }

    /**
     * Unit tests the <tt>BreadthFirstDirectedPaths</tt> data type.
     */
    public static void main(String[] args) {

        DiGraph<Integer> graph = new DiGraph<Integer>();

        graph.add(0);
        graph.add(1);
        graph.add(2);
        graph.add(3);
        graph.add(4);
        graph.add(5);
        graph.add(6);
        graph.add(7);
        graph.add(8);

        graph.add(1, 0, 1);
        graph.add(1, 2, 1);
        graph.add(3, 2, 1);
        graph.add(4, 3, 1);
        graph.add(5, 4, 1);
        graph.add(6, 5, 1);
        graph.add(7, 6, 1);
        graph.add(7, 0, 1);
        graph.add(0, 8, 1);
        graph.add(2, 8, 1);
        graph.add(4, 8, 1);
        graph.add(5, 8, 1);
        graph.add(6, 8, 1);
        graph.add(8, 1, 1);
        graph.add(8, 3, 1);
        graph.add(8, 7, 1);
        int s = 0;
        BFS bfs = new BFS(graph, s);

        for (int v = 0; v < graph.getNumberVertices(); v++) {
            if (bfs.hasPathTo(v)) {
                System.out.printf("%d to %d (%d):  ", s, v, bfs.distTo(v));
                for (int x : bfs.pathTo(v)) {
                    if (x == s) System.out.print(x);
                    else        System.out.print(x + "<-");
                }
                System.out.println();
            }

            else {
                System.out.printf("%d to %d (-):  not connected\n", s, v);
            }

        }
    }


}