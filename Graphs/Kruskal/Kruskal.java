package Kruskal;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class Kruskal {

    public static void main(String[] args) {
        Graph graph = new Graph();
        graph.addVertices(8);

//        graph.incident(0, 1, 4); // a -> b
//        graph.incident(0, 8, 8); // a -> h
//        graph.incident(1, 2, 8); // b -> c
//        graph.incident(1, 8, 11); // b -> h
//        graph.incident(2, 3, 7); // c -> d
//        graph.incident(2, 5, 4); // c -> f
//        graph.incident(2, 7, 2); // c -> i
//        graph.incident(3, 4, 9); // d -> e
//        graph.incident(3, 5, 14); // d -> f
//        graph.incident(4, 5, 10); // e -> f
//        graph.incident(5, 6, 2); // f -> g
//        graph.incident(6, 7, 6); // g -> i
//        graph.incident(6, 8, 1); // g -> h
//        graph.incident(7, 8, 7); // i -> h
        graph.incident(0, 4, 8); // a -> e
        graph.incident(0, 5, 10); // a -> f
        graph.incident(0, 1, 17); // a -> b
        graph.incident(1, 6, -5); // b -> g
        graph.incident(1, 2, 19); // b -> c
        graph.incident(5, 6, 25); // f -> g
        graph.incident(5, 7, -10); // f -> h
        graph.incident(4, 3, 6); // e -> d
        graph.incident(4, 7, -4); // e -> h
        graph.incident(3, 7, 1); // d -> h
        graph.incident(3, 2, 2); // d -> c
        graph.incident(6, 7, -12); // g -> h
        graph.incident(6, 2, -3); // g -> c

        System.out.println("######### Initial graph: #########" + graph);
        System.out.println("");

        Kruskal(graph);

    }

    public static ArrayList<Edge> Kruskal(Graph g) {
        ArrayList<Edge> A = new ArrayList<>();
        Sets set = new Sets();
        set.pathCompression = true;
        for (Vertex n : g.vertices) {
            set.makeSet(n);
        }
        PriorityQueue<Edge> pq = new PriorityQueue();
        for (Edge e : g.edges) {
            pq.add(e);
        }
        System.out.println("######### Steps #########");
        int pqSize = pq.size();
        for (int i = 0; i < pqSize; i++) {
            Edge e = pq.poll();
            if (set.findSet(e.vertexFrom) != set.findSet(e.vertexTo)) {
                A.add(e);
                set.union(e.vertexFrom, e.vertexTo);
            }
        }
        System.out.println("\n######### Taking out of Priority Queue in order: #########");
        System.out.println("############# (only edges chosen in Kruskals) ############");
        
        for (Edge e : A) {
            System.out.printf("(%s, %s)\n", (char) e.vertexFrom.name, (char) e.vertexTo.name);
        }
        printSolution(g, set, A);
        return A;
    }

    public static void printSolution(Graph graph, Sets set, ArrayList<Edge> endArray) {
        System.out.println("\n######### Resulting edges in the forest #########");
        ArrayList<Vertex> parents = new ArrayList();
        for (Edge e : endArray) {
            System.out.printf("edge: %s, \n\tparent of %s: %s, \n\tparent of %s: %s\n", e, (char) e.vertexFrom.name, set.findSet(e.vertexFrom), (char) e.vertexTo.name, set.findSet(e.vertexTo));
            if (!parents.contains(set.findSet(e.vertexFrom))) {
                parents.add(set.findSet(e.vertexFrom));
            }
            if (!parents.contains(set.findSet(e.vertexTo))) {
                parents.add(set.findSet(e.vertexTo));
            }
        }
        System.out.printf("\n######### Roots in forest: #########\n");
        for (Vertex v : parents) {
            System.out.printf("\t%s, Rank: %s\n", v, v.rank);
        }
    }

}
