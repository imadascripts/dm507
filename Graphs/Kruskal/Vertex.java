package Kruskal;

import java.util.List;
import java.util.LinkedList;

public class Vertex {
    int name;
    public List<Edge> adj;
    public int rank;
    public Vertex parent;

    public Vertex(int i) {
        this.name = i+65;
        this.adj = new LinkedList<>();
    }
    
    @Override
    public String toString() {
        return String.format("Vertex %s", (char) name);
    }
    
//    public String parentString() {
//        return String.format("%s %s" , name, parent != null? "<- "+parent.parentString():"");
//    }
}
