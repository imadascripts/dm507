package Kruskal;

public class Edge implements Comparable<Edge>{
    public int w = 1;
    public Vertex vertexFrom;
    public Vertex vertexTo;

    public Edge(Vertex v1, Vertex v2) {
        this.vertexFrom = v1;
        this.vertexTo = v2;
        
    }

    public Edge(Vertex v1, Vertex v2, int w) {
        this(v1, v2);
        this.w = w;
    }

    @Override
    public int compareTo(Edge o) {
        int r = this.w-o.w;
        return r;    
    }
    
    @Override
    public String toString() {
        return String.format("(%s)",w);
    }

}
