package Kruskal;

public class Sets {

    public boolean pathCompression = true;

    public void makeSet(Vertex n) {
        n.parent = n;
        n.rank = 0;
    }

    public Vertex findSet(Vertex n) {
        if (pathCompression == true) {
            if (n.parent != n) {
                n.parent = findSet(n.parent);
            }
            return n.parent;
        } else {
            if (n.parent == n) {
                return n;
            } else {
                return (findSet(n.parent));
            }
        }
    }

    public void union(Vertex x, Vertex y) {
        Vertex xRoot = findSet(x);
        Vertex yRoot = findSet(y);
        if (xRoot == yRoot) {
            return;
        }
        if (xRoot.rank < yRoot.rank) {
            xRoot.parent = yRoot;
            System.out.printf("\nParent of %s is set to %s\n", x, yRoot);
        } else if (xRoot.rank > yRoot.rank) {
            yRoot.parent = xRoot;
            System.out.printf("\nParent of %s is set to %s\n", y, xRoot);
        } else {
            if (xRoot.name < yRoot.name) {
                yRoot.parent = xRoot;
                xRoot.rank++;
                System.out.printf("\nParent of %s is set to %s\n\tNew rank: %s\n", y, xRoot, xRoot.rank);
            } else if (xRoot.name > yRoot.name) {
                xRoot.parent = yRoot;
                yRoot.rank++;
                System.out.printf("\nParent of %s is set to %s\n\tNew rank: %s\n", x, yRoot, yRoot.rank);
            }
        }

    }
}
