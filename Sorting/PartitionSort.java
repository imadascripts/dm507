import java.util.ArrayList;

public class PartitionSort {

    // Operates with values A,p,r as when the sort is called with
    // PARTITION(A,p,r)
    public static void main(String[] args) {

        // array for partionsort to sort
        ArrayList<Integer> A = new ArrayList<>();

        // make array A
        A.add(4);
        A.add(7);
        A.add(1);
        A.add(5);
        A.add(8);
        A.add(2);
        A.add(5);
        A.add(5);

        // Choose p and r:
        int p = 1;
        int r = 8;
        
        int[] returnArray = Partition(A,p-1,r-1);
        System.out.printf("Return value: %s on index %s\n", returnArray[1], returnArray[0]);
    }

    public static int[] Partition(ArrayList<Integer> A, int p, int r) {
        int i = p-1;
        int x = (int) A.get(r);

        for (int j = p; j <= r-1; j++) {
            if ((int) A.get(j) <= x) {
                i++;
                int temp = (int) A.get(i);
                A.set(i, A.get(j));
                A.set(j, temp);
            }
        }
        int temp2 = (int) A.get(i + 1);
        A.set(i+1, A.get(r));
        A.set(r, temp2);
        System.out.println("Final array: " + A);
        int[] returnArray = new int[2];
        returnArray[0] = i+1;
        returnArray[1] = (int) A.get(i+1);
        return returnArray;
    }
}
