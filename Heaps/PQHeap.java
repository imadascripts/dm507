
import java.util.ArrayList;

public class PQHeap implements PQ {

    private final ArrayList<Integer> A;
    private final int maxElms;
    
    public PQHeap(int maxElms) {
        this.A = new ArrayList<>();
        this.maxElms = maxElms;
    }
    
    public static void main(String[] args) {
        // make new priorityqueue of size 10
        PQ pq = new PQHeap(10);
        
        // insert integers in pq
        pq.insert(1);
        pq.insert(2);
        pq.insert(5);
        pq.insert(3);
        pq.insert(7);
        pq.insert(9);
        pq.insert(6);
        pq.insert(8);
        pq.insert(4);
        pq.insert(10);
        
        //print pq, before and after extractMin()
        System.out.println(pq);
        pq.extractMin();
        System.out.println(pq);
    }

//    @Override
//    public String toString() {
//        return String.format("%d: %s", maxElms, A);
//    }

    public static int parent(int i) {
        return i;
    }

    public static int left(int i) {
        return 2 * i + 1;
    }

    public static int right(int i) {
        return 2 * i + 2;
    }

    public static void buildMinHeap(ArrayList A) {
        int heapSize = A.size();
        for (int i = heapSize / 2 - 1; i >= 0; i--) {
            minHeapify(A, i);
        }
    }

    public static void minHeapify(ArrayList<Integer> A, int i) {
        int l = left(i);
        int r = right(i);
        int p = parent(i);
        int smallest;
        if ((l < A.size()) && (A.get(l) < A.get(p))) {
            smallest = l;
        } else {
            smallest = p;
        }
        if ((r < A.size()) && (A.get(r) < A.get(smallest))) {
            smallest = r;
        }
        if (smallest != p) {
            Integer temporary = A.get(p);
            A.set(p, A.get(smallest));
            A.set(smallest, temporary);
            minHeapify(A, smallest);
        }
    }

    @Override
    public Integer extractMin() {
        if (A.isEmpty()) {
            return null;
        }
        Integer minInt = A.remove(0);
        if (A.size() > 1) {
            A.add(0, A.remove(A.size() - 1));
            buildMinHeap(A);
        }
        return minInt;
    }

    @Override
    public void insert(Integer e) { 
        if (A.size() < maxElms) {
            A.add(e);
            buildMinHeap(A);
        }
    }
    
    @Override
    public String toString() {
        ArrayList<Integer> strArray = new ArrayList<>();
        for (Integer e : A) {
            strArray.add(e);
        }
        return strArray.toString();
    }

}
